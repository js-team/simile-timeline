Source: simile-timeline
Section: javascript
Priority: optional
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders: Chris Lamb <lamby@debian.org>, Martin <debacle@debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13), ant, yui-compressor
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/js-team/simile-timeline
Vcs-Git: https://salsa.debian.org/js-team/simile-timeline.git
Homepage: https://www.simile-widgets.org/

Package: libjs-simile-timeline
Architecture: all
Depends: ${misc:Depends}
Recommends: javascript-common, libjs-jquery
Multi-Arch: foreign
Description: JavaScript library for web-based interactive timelines
 Timeline is a DHTML-based AJAX-based widget for visualizing time-based
 events. It uses the "slippy page" concept in a similar way to Google Maps
 allowing the user to pan the timeline by dragging the page horizontally.
 .
 Each timeline can consist of multiple "bands", each with a configurable
 scale, which can provide useful context when displaying a large number
 of items. Timelines can be populated directly via XML or JSON.
